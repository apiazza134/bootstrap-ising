#!/bin/bash

source /etc/profile
# source $HOME/.profile

module load gnu8
module load openmpi3
module load mathematica
module load python3/3.8

export LIBRARY_PATH="$HOME/.local/lib:$LIBRARY_PATH"
export LD_LIBRARY_PATH="$HOME/.local/lib:$HOME/.local/lib64:$LD_LIBRARY_PATH"

# export phys_cores_per_node=$(lscpu | awk '/^Core.s. per socket:/ {cores=$NF} /^Socket.s.:/ {sockets=$NF} END {print cores * sockets}')

export phys_cores_per_node=${SLURM_NTASKS:-1}

ulimit -n 65536

echo "DEBUG            $phys_cores_per_node"
