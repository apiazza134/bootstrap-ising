#!/bin/bash

export SLURM_MMAFILE=$1
export SLURM_MMAPARAM=$2
export SLURM_PROJECTDIR=$3

mkdir -p $SLURM_PROJECTDIR

source /etc/profile.d/modules.sh
module load slurm


sbatch  --account $(whoami) --output=./$SLURM_PROJECTDIR/btjob-%j.out ./Scripts/RunMMA_job.sh
