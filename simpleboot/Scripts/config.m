(* ::Package:: *)

 Cluster$Configuration = {
    "[Cluster.LoginServer]" -> "frontend1.hpc.sissa.it",   (* The address of your cluster *)
    "[Cluster.Account]" -> "apiazza",  (* Your account. Simpleboot will use "ssh [Cluster.Account]@[Cluster.LoginServer]" to connect to the cluster  *)
    "[Cluster.WorkspaceDirectory]" -> "/home/apiazza/simpleboot/Workspace",     (* Workspace directory on the cluster *)
    "[Cluster.PackageDirectory]" -> "/home/apiazza/simpleboot/Packages",     (* Packages directory on the cluster *)
    "[Local.PackageDirectory]" -> "/home/alepiazza/phd/projects/simpleboot/Packages", (* Packages directory on the your laptop. If you prefer to use dropbox/OneDrive to host the file and use multiple computers to work on the project, you may set an environment variable for it. *)
    
    "[AutoCB3.scalar_blocks_mod.script]"->
    "mpirun --bind-to none -n 1 /home/apiazza/2023-perimeter/packages/scalar-blocks-mod/install/bin/scalar_blocks_mod --num-threads $phys_cores_per_node", (* The script to call scalar_blocks_mod *)
    "[AutoCB3.sdp2input_mod.script]"->
    "mpirun -n $phys_cores_per_node --bind-to none /home/apiazza/2023-perimeter/packages/sdp2input_mod/install/bin/sdp2input_mod_2.4.0 ", (* The script to call sdp2input_mod *)
    "[AutoCB3.sdpdd]"->
    "mpirun -n $phys_cores_per_node /home/apiazza/2023-perimeter/packages/sdpdd_bBcA_hessian/install/bin/sdpdd_hessian_ij --procsPerNode=$phys_cores_per_node ", (* The script to call sdpdd *)
    "[sdpb.script]"->
    "mpirun -n $phys_cores_per_node /home/apiazza/2023-perimeter/packages/sdpb2.4.0_midck_stallingrecover/install/bin/sdpb2.4.0_midck --procsPerNode $phys_cores_per_node", (* The script to call SDPB *)
    "[DynamicSDPB.script]"->
    "mpirun -n $phys_cores_per_node /home/apiazza/2023-perimeter/packages/sdpb_skydiving/install/bin/dynamical_sdp_RV1B", (* The script to call skydiving *)
    
    "[AutoCB3.spectrumpy]"-> "source $HOME/repos/spectrum-extraction/venv/bin/activate && $HOME/repos/spectrum-extraction/spectrum.py",
    
    (* no need to modify the items below *)
    "[ClusterModel]" -> "srun",
    "[SDPB.Version]"->"sdpb_El",
    "[QUADRATICNET.SCRIPT]"->"$HOME/package/stack/stack-2.3.1-linux-x86_64/stack exec -- quadratic-net-exe --pvm2sdpExecutable /home/yhe/package/quadratic-net/scripts/pvm2sdp.sh --sdpbExecutable /home/yhe/package/quadratic-net/scripts/sdpb.sh  --workDir tmp",
    "[QUADRATICNET.SWITCH]"->False,
    
    "[UsePackage.scalar_block]"->False,
    "[UsePackage.sdp2input]"->False,
    "[Debug.TimingStatistics]"->True,
    
    (* AutoCB3 configuration *)
    "[UsePackage.AutoCB3]"->True,
    "[AutoCB3.no-fake-poles]"->True,
    "[AutoCB3.sdp2input_mod]"->True,
    "[AutoCB3.scalar_blocks_mod.cmdprec]"->120,
    "[AutoCB3.scalar_blocks_mod.precision]"->1024,
    "[AutoCB3.sdp2input_mod.precision]"->1024
}


ReconfigCmd[str_]:=FixedPoint[StringReplace[#,Cluster$Configuration]&,str];
SSH$ReconfigCmd=ReconfigCmd;
